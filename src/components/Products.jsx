import React from "react";
import { Button, Card, CardActions, CardContent, CardMedia, Typography } from '@mui/material';

export default function Products({ state, dispatch }) {
    const { products, cart } = state;
    return (
        <div
            style={{
                display: "flex",
                flexWrap: "wrap",
                justifyContent: "space-evenly",
                width: "80%"
            }}
        >
            {products.map((prod) => (
                <Card sx={{ maxWidth: 345 }}
                    style={{
                        display: "flex",
                        flexDirection: "column",
                        padding: 20,
                        marginTop: 20,
                        gap: 10,
                        border: "1px solid grey",
                    }}
                    key={prod.id}
                >
                    <CardMedia
                        component="img"
                        alt={prod.title}
                        height="200"
                        image={prod.thumbnail}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h6" component="div">
                            {prod.title}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            {prod.description.slice(0, 50) + (prod.description.length > 50 ? "..." : "")}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        {cart.some((p) => p.id === prod.id) ? (
                            <Button variant="contained" color="error" onClick={() =>
                                dispatch({
                                    type: "REMOVE_FROM_CART",
                                    payload: prod
                                })
                            }>
                                Remove
                            </Button>
                        ) : (

                            <Button variant="contained" color="success" onClick={() =>
                                dispatch({
                                    type: "ADD_TO_CART",
                                    payload: {
                                        id: prod.id,
                                        title: prod.title,
                                        thumbnail: prod.thumbnail,
                                        qty: 1,
                                        price: prod.price
                                    }
                                })
                            }>
                                Add
                            </Button>
                        )}
                        <Button size="small">Learn More</Button>
                    </CardActions>
                </Card>
            ))}
        </div>

    );
}
