import axios from "axios";
import { useEffect, useReducer } from "react";
import Products from "../components/Products";
import Cart from "../components/Cart";
import { CartReducer } from "../reducers/CartReducer";

export default function App() {
  const [state, dispatch] = useReducer(CartReducer, {
    products: [],
    cart: []
  });

  const fetchProducts = async () => {
    const {
      data: { products }
    } = await axios.get("https://dummyjson.com/products");
    dispatch({
      type: "ADD_PRODUCTS",
      payload: products
    });
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div style={{ display: "flex" }}>
      <Products state={state} dispatch={dispatch} />
      <Cart state={state} dispatch={dispatch} />
    </div>
  );
}
