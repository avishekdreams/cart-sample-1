import React, { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import axios from 'axios';

function User() {
    const [user, setUser] = useState({});
    const id = useLocation().pathname.split("/")[2];

    useEffect(() => {
        const cancelToken = axios.CancelToken.source();
        axios.get(`https://jsonplaceholder.typicode.com/users/${id}`, { cancelToken: cancelToken.token })
            .then((res) => {
                setUser(res.data)
            }).catch(err => {
                if (axios.isCancel(err)) {
                    console.log("Request cancelled");
                }
            })

        return () => {
            cancelToken.cancel();
        }
    }, [id]);

    return (
        <div>
            <p>Name: {user.name}</p>
            <p>User: {user.username}</p>
            <p>Email: {user.email}</p>
            <Link to="/users/1">Fetch User 1</Link><br />
            <Link to="/users/2">Fetch User 2</Link><br />
            <Link to="/users/3">Fetch User 3</Link>
        </div>
    )
}

export default User;